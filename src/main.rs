use std::f64;

const EXPONENT_BIAS: i16 = 1023;
const MANTISSA_SHIFT: i16 = 52;

const CAIRO_FIXED_FRAC_BITS: u64 = 8;
const CAIRO_MAGIC_NUMBER_FIXED: f64 = (1u64 << (52 - CAIRO_FIXED_FRAC_BITS)) as f64 * 1.5;

const CAIRO_FIXED_ONE_DOUBLE: f64 = (1 << CAIRO_FIXED_FRAC_BITS) as f64;

fn unbias_exponent(e: i16) -> i16 {
    e - (EXPONENT_BIAS + MANTISSA_SHIFT)
}

fn bias_exponent(e: i16) -> i16 {
    e + (EXPONENT_BIAS + MANTISSA_SHIFT)
}

fn cairo_magic_double(d: f64) -> f64 {
    d + CAIRO_MAGIC_NUMBER_FIXED
}

fn cairo_fixed_from_double(d: f64) -> i32 {
    let bits = cairo_magic_double(d).to_bits();
    let lower = bits & 0xffffffff;
    lower as i32
}

fn cairo_fixed_to_double(f: i32) -> f64 {
    f as f64 / CAIRO_FIXED_ONE_DOUBLE
}

/// Returns the mantissa, unbiased exponent and sign as integers.
/// This comes from
/// https://github.com/rust-lang/rust/blob/556fb02e/src/libcore/num/dec2flt/rawfp.rs#L209-L222
fn integer_decode(val: f64) -> (u64, i16, i8) {
    let bits = val.to_bits();
    let sign: i8 = if bits >> 63 == 0 { 1 } else { -1 };
    let mut exponent: i16 = ((bits >> 52) & 0x7ff) as i16;
    let mantissa = if exponent == 0 {
        (bits & 0xfffffffffffff) << 1
    } else {
        (bits & 0xfffffffffffff) | 0x10000000000000
    };
    exponent = unbias_exponent(exponent);
    (mantissa, exponent, sign)
}

fn print_f64(prefix: &str, val: f64) {
    println!("{}", prefix);

    let bits = val.to_bits();
    println!("{b:064b} ({b:#x})", b = bits);
    println!("seeeeeeeeeeemmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");

    let (mant, unbiased_exp, sign) = integer_decode(val);
    println!("mantissa: {m:#x}  unbiased exponent: {ube}  exponent: {be}  sign: {s}\n",
             m = mant,
             ube = unbiased_exp,
             be = bias_exponent(unbiased_exp),
             s = sign);

}

fn print_fixed(d: f64) {
    println!("fixed {} = {:#010x}", d, cairo_fixed_from_double(d));
    print_f64("with magic", cairo_magic_double(d));
}

fn main() {
    print_f64("1.0", 1.0);

    print_f64("0.0", 0.0);

    print_f64("-0.0", -0.0);

    print_f64("+Infinity", f64::INFINITY);

    print_f64("-Infinity", f64::NEG_INFINITY);

    print_f64("NaN", f64::NAN);

    print_f64("EPSILON", f64::EPSILON);

    print_f64("MIN_POSITIVE", f64::MIN_POSITIVE);

    print_f64("MAX", f64::MAX);
    print_f64("MIN", f64::MIN);

    print_fixed(0.0);
    print_fixed(1.0);

    print_f64("-1.0", -1.0);
    print_fixed(-1.0);

    print_fixed(cairo_fixed_to_double(cairo_fixed_from_double(1.0)));

    print_f64("max fixed", cairo_fixed_to_double(0x7fffffffu32 as i32));
    print_f64("min fixed", cairo_fixed_to_double(0x80000000u32 as i32));
    print_fixed(cairo_fixed_to_double(0x7fffffffu32 as i32));
    print_fixed(cairo_fixed_to_double(0x80000000u32 as i32));
}
